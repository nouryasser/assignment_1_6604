/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment1;

/**
 *
 * @author nouryasser
 */
public class Activity {

    private String name;
    private int caloriesBurnt=0;
    private double heartRate=80;
    private double hrInc=0;
    private int time;
    int xCalories=0;
    double xheartRate=0;

    public Activity(String name, int time) {
        this.name = name;
        this.time = time;
    }
    public void setValues(String name,int time){
    this.name=name;
    this.time=time;
    }
    public String getName(){
        return name;
    }
    public void setHR(double heartRate){
    this.heartRate=heartRate;
    }
    public double getHR(){
        return heartRate;
    }
    public void setCalories(int caloriesBurnt){
        this.caloriesBurnt=caloriesBurnt;
    }
    public int getCalories(){
        return caloriesBurnt;
    }
    public void setInc(double hrInc){
        this.hrInc=hrInc;
    }
    public double getInc(){
        return hrInc;
    }

    public int calculateCalories(int time, int factor) {
        caloriesBurnt = caloriesBurnt + time * factor;
        return caloriesBurnt;
    }

    public double calculateHR(int time, double factorhr) {
        heartRate = heartRate + (heartRate * time * factorhr);
        return heartRate;
    }
}
